﻿#region usings
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace ContractorsConfig.Repository.Api
{
    public class DepartureToCompanyRepository : IDepartureToCompanyRepository
    {
        #region private fields
        private readonly IBaseApiRepository _baseApiRepository;
        #endregion

        #region cstr

        public DepartureToCompanyRepository(IBaseApiRepository baseApiRepository)
        {
            _baseApiRepository = baseApiRepository;
        }
        #endregion

        #region public methods
        public async Task<DepartureToCompany> GetByClientIdAndCompanyId(Guid clientId, Guid companyId)
        {
            var result = await _baseApiRepository.MakeRequest<DepartureToCompany>(
                collectionName: "departureToClient",
                args: new Dictionary<string, string> 
                { 
                    { "clientId", clientId.ToString() },
                    { "companyId", companyId.ToString() }, 
                });

            //Make request triggers external api filters - check if its works to be sure
            return result != null && result.ClientId == clientId && result.CompanyId == companyId
                ? result
                : null;
        }
        #endregion
    }
}
