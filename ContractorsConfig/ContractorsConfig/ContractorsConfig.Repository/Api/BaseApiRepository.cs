﻿#region usings

using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using ContractorsConfig.Core;
using ContractorsConfig.Repository.Interfaces;

#endregion

namespace ContractorsConfig.Repository.Api
{
    public class BaseApiRepository : IBaseApiRepository
    {
        #region private fields
        private readonly IWebRequest _webRequest;
        #endregion

        #region cstr
        public BaseApiRepository(IWebRequest webRequest)
        {
            _webRequest = webRequest;
        }
        #endregion

        #region public methods
        public async Task<T> MakeRequest<T>(string collectionName, IDictionary<string, string> args) where T : class
        {
            try
            {
                var request = _webRequest.Request(collectionName, args);
                using (var response = await request.GetResponseAsync() as HttpWebResponse)
                {
                    if (response != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            var jsonSerializer = new DataContractJsonSerializer(typeof(T));
                            var responseStream = response.GetResponseStream();

                            if (responseStream != null)
                            {
                                var jsonResponse = jsonSerializer.ReadObject(responseStream) as T;
                                return jsonResponse;
                            }
                        }

                        throw new Exception(
                            message: string.Format("REST API ERROR (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription)
                            );
                    }

                    throw new Exception(message: "REST API ERROR RESPONSE IS NULL");
                }
            }
            catch (Exception)
            {
                //log
                return null;
            }
        }
        #endregion
    }
}
