﻿#region usings
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
#endregion

namespace ContractorsConfig.Repository.Api
{
    public class DepartureToClientRepository : IDepartureToClientRepository
    {
        #region private fields
        private readonly IBaseApiRepository _baseApiRepository;
        #endregion

        #region cstr

        public DepartureToClientRepository(IBaseApiRepository baseApiRepository)
        {
            _baseApiRepository = baseApiRepository;
        }
        #endregion

        #region public methods
        public async Task<DepartureToClient> GetByClientId(Guid clientId)
        {
            var result = await _baseApiRepository.MakeRequest<DepartureToClient>(
                collectionName: "departureToClient",
                args: new Dictionary<string, string> { { "clientId", clientId.ToString() } }
                );

            //Make request triggers external api filters - check if its works to be sure
            return result != null && result.ClientId == clientId ? result : null;
        }
        #endregion
    }
}
