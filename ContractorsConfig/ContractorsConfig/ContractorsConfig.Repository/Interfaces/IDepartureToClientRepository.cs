﻿using ContractorsConfig.Model;
using System;
using System.Threading.Tasks;

namespace ContractorsConfig.Repository.Interfaces
{
    public interface IDepartureToClientRepository
    {
        Task<DepartureToClient> GetByClientId(Guid clientId);
    }
}
