﻿using System.Collections.Generic;

namespace ContractorsConfig.Repository.Interfaces
{
    public interface IBaseFileRepository<T>
    {
        IEnumerable<T> DataCollecion { get; set; }
    }
}
