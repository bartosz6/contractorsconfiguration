﻿using ContractorsConfig.Model;
using System;
using System.Threading.Tasks;

namespace ContractorsConfig.Repository.Interfaces
{
    public interface IDepartureToCompanyRepository
    {
        Task<DepartureToCompany> GetByClientIdAndCompanyId(Guid clientId, Guid companyId);
    }
}
