﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractorsConfig.Repository.Interfaces
{
    public interface IBaseApiRepository
    {
        Task<T> MakeRequest<T>(string collectionName, IDictionary<string, string> args) where T : class;
    }
}
