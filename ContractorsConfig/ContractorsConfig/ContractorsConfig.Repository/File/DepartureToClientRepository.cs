﻿#region usings
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks; 
#endregion

namespace ContractorsConfig.Repository.File
{
    public class DepartureToClientRepository : IDepartureToClientRepository
    {
        #region private fields
        private readonly IBaseFileRepository<DepartureToClient> _baseFileRepository;
        #endregion

        #region cstr

        public DepartureToClientRepository(IBaseFileRepository<DepartureToClient> baseFileRepository)
        {
            _baseFileRepository = baseFileRepository;
        } 
        #endregion

        #region public methods
        public async Task<DepartureToClient> GetByClientId(Guid clientId)
        {
            var task = new Task<DepartureToClient>(() =>
                _baseFileRepository.DataCollecion.FirstOrDefault(a => a.ClientId == clientId));

            task.Start();

            return await task;
        } 
        #endregion
    }
}
