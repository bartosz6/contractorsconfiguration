﻿#region usings

using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using ContractorsConfig.Core;
using ContractorsConfig.Model.Interfaces;
using ContractorsConfig.Repository.Interfaces;

#endregion

namespace ContractorsConfig.Repository.File
{
    public class BaseFileRepository<T> : IBaseFileRepository<T>  where T : class
    {
        #region protected fields
        public IEnumerable<T> DataCollecion { get; set; }
        #endregion

        #region private fields
        private readonly IConfigurationStorage _config; 
        #endregion

        #region cstr
        public BaseFileRepository(IConfigurationStorage configurationStorage)
        {
            _config = configurationStorage;
            DataCollecion = ReadDataFromConfig();
        } 
        #endregion

        #region private methods
        private IEnumerable<T> ReadDataFromConfig()
        {
            var dataFromConfigFile = _config.GetConfigString();
            if (!string.IsNullOrWhiteSpace(dataFromConfigFile))
            {
                using (var s = StreamHelper.GenerateStreamFromString(dataFromConfigFile.Replace('\'', '\"')))
                {
                    var jsonSerializer = new DataContractJsonSerializer(typeof(IEnumerable<T>));
                    var jsonObject = jsonSerializer.ReadObject(s) as IEnumerable<T>;
                    return jsonObject;
                }
            }

            throw new Exception("Brak danych w pliku .config");
        } 
        #endregion
    }
}
