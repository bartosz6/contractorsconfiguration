﻿#region usings
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
#endregion

namespace ContractorsConfig.Repository.File
{
    public class DepartureToCompanyRepository : IDepartureToCompanyRepository
    {
        #region private fields
        private readonly IBaseFileRepository<DepartureToCompany> _baseFileRepository;
        #endregion

        #region cstr

        public DepartureToCompanyRepository(IBaseFileRepository<DepartureToCompany> baseFileRepository)
        {
            _baseFileRepository = baseFileRepository;
        }
        #endregion

        #region public methods
        public async Task<DepartureToCompany> GetByClientIdAndCompanyId(Guid clientId, Guid companyId)
        {
            var task = new Task<DepartureToCompany>(() =>
                _baseFileRepository.DataCollecion.FirstOrDefault(a => a.ClientId == clientId && a.CompanyId == companyId));

            task.Start();

            return await task;
        }
        #endregion
    }
}
