﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ContractorsConfig.Repository.Interfaces;
using ContractorsConfig.Service;
using ContractorsConfig.Model;
using ContractorsConfig.Service.Models;
using System.Threading.Tasks;

namespace ContractorsConfig.Tests.Unit.Service
{
    [TestClass]
    public class ContractorsConfigLogicTests
    {
        [TestMethod]
        public async Task GetDepartureTimeByClientIdAndCompanyId_Both_departures_found_both_has_leavetime_should_return_departure_time_from_departure_to_client()
        {          
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");

            var clientRepo = new Mock<IDepartureToClientRepository>();
            var companyRepo = new Mock<IDepartureToCompanyRepository>();

            var service = new ContractorsConfigLogic(clientRepo.Object, companyRepo.Object);

            var clientObj = new DepartureToClient {
                ClientId = clientId,
                LeaveFrom = TimeSpan.Parse("8:00"),
                LeaveTo = TimeSpan.Parse("8:20"),
                IsSpecial = false
            };
            var companyObj = new DepartureToCompany {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = companyId,
                LeaveFrom = TimeSpan.Parse("9:00"),
                LeaveTo = TimeSpan.Parse("9:25"),
                IsSpecial = false
            };

            var expected = new DepartureTimeModel
            {
                LeaveFrom = clientObj.LeaveFrom,
                LeaveTo = clientObj.LeaveTo,
            };

            clientRepo
                .Setup(a => a.GetByClientId(clientId))
                .ReturnsAsync(clientObj);
            companyRepo
                .Setup(a => a.GetByClientIdAndCompanyId(clientId, companyId))
                .ReturnsAsync(companyObj);


            var result = await service.GetDepartureTimeByClientIdAndCompanyId(clientId, companyId);


            Assert.AreEqual(expected.LeaveFrom, result.LeaveFrom);
            Assert.AreEqual(expected.LeaveTo, result.LeaveTo);
        }

        [TestMethod]
        public async Task GetDepartureTimeByClientIdAndCompanyId_Both_departures_found_departure_to_client_has_not_got_leavetime_should_return_departure_time_from_departure_to_company()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");

            var clientRepo = new Mock<IDepartureToClientRepository>();
            var companyRepo = new Mock<IDepartureToCompanyRepository>();

            var service = new ContractorsConfigLogic(clientRepo.Object, companyRepo.Object);

            var clientObj = new DepartureToClient
            {
                ClientId = clientId,
                IsSpecial = false
            };
            var companyObj = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = companyId,
                LeaveFrom = TimeSpan.Parse("9:00"),
                LeaveTo = TimeSpan.Parse("9:25"),
                IsSpecial = false
            };

            var expected = new DepartureTimeModel
            {
                LeaveFrom = companyObj.LeaveFrom,
                LeaveTo = companyObj.LeaveTo,
            };

            clientRepo
                .Setup(a => a.GetByClientId(clientId))
                .ReturnsAsync(clientObj);
            companyRepo
                .Setup(a => a.GetByClientIdAndCompanyId(clientId, companyId))
                .ReturnsAsync(companyObj);


            var result = await service.GetDepartureTimeByClientIdAndCompanyId(clientId, companyId);


            Assert.AreEqual(expected.LeaveFrom, result.LeaveFrom);
            Assert.AreEqual(expected.LeaveTo, result.LeaveTo);
        }

        [TestMethod]
        public async Task GetDepartureTimeByClientIdAndCompanyId_Cannot_find_company_should_return_null()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");

            var clientRepo = new Mock<IDepartureToClientRepository>();
            var companyRepo = new Mock<IDepartureToCompanyRepository>();

            var service = new ContractorsConfigLogic(clientRepo.Object, companyRepo.Object);

            var clientObj = new DepartureToClient
            {
                ClientId = clientId,
                IsSpecial = false
            };

            clientRepo
                .Setup(a => a.GetByClientId(clientId))
                .ReturnsAsync(clientObj);
            companyRepo
                .Setup(a => a.GetByClientIdAndCompanyId(clientId, companyId))
                .ReturnsAsync(null);


            var result = await service.GetDepartureTimeByClientIdAndCompanyId(clientId, companyId);


            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public async Task GetDepartureTimeByClientIdAndCompanyId_Cannot_find_client_should_return_null()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");

            var clientRepo = new Mock<IDepartureToClientRepository>();
            var companyRepo = new Mock<IDepartureToCompanyRepository>();

            var service = new ContractorsConfigLogic(clientRepo.Object, companyRepo.Object);

            var companyObj = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = companyId,
                LeaveFrom = TimeSpan.Parse("9:00"),
                LeaveTo = TimeSpan.Parse("9:25"),
                IsSpecial = false
            };
        
            clientRepo
                .Setup(a => a.GetByClientId(clientId))
                .ReturnsAsync(null);
            companyRepo
                .Setup(a => a.GetByClientIdAndCompanyId(clientId, companyId))
                .ReturnsAsync(companyObj);


            var result = await service.GetDepartureTimeByClientIdAndCompanyId(clientId, companyId);


            Assert.AreEqual(null, result);
        }

        [TestMethod]
        public async Task GetDepartureTimeByClientIdAndCompanyId_Both_departures_both_without_leave_times_should_return_null()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");

            var clientRepo = new Mock<IDepartureToClientRepository>();
            var companyRepo = new Mock<IDepartureToCompanyRepository>();

            var service = new ContractorsConfigLogic(clientRepo.Object, companyRepo.Object);

            var clientObj = new DepartureToClient
            {
                ClientId = clientId,
                IsSpecial = false
            };
            var companyObj = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = companyId,
                IsSpecial = false
            };

            clientRepo
                .Setup(a => a.GetByClientId(clientId))
                .ReturnsAsync(clientObj);
            companyRepo
                .Setup(a => a.GetByClientIdAndCompanyId(clientId, companyId))
                .ReturnsAsync(companyObj);


            var result = await service.GetDepartureTimeByClientIdAndCompanyId(clientId, companyId);


            Assert.AreEqual(null, result);
        }    
    }
}
