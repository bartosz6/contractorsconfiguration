﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Api;
using ContractorsConfig.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Unit.Repository.Api
{
    [TestClass]
    public class DepartureToClientRepositoryTests
    {
        [TestMethod]
        public async Task GetByClientId_Should_Filter_By_ClientId()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var expectedFromMethod = new DepartureToClient
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var returnedFromBase = new DepartureToClient
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var collectionName = "departureToClient";
            var args = new Dictionary<string, string>
            {
                { "clientId", clientId.ToString() }
            };
            var baseRepo = new Mock<IBaseApiRepository>();
            baseRepo
                .Setup(a => a.MakeRequest<DepartureToClient>(collectionName, args))
                .ReturnsAsync(returnedFromBase);

            var repo = new DepartureToClientRepository(baseRepo.Object);

            var requestResult = await repo.GetByClientId(clientId);

            Assert.AreEqual(expectedFromMethod.ClientId, requestResult.ClientId);
            Assert.AreEqual(expectedFromMethod.LeaveFrom, requestResult.LeaveFrom);
            Assert.AreEqual(expectedFromMethod.LeaveTo, requestResult.LeaveTo);
            Assert.AreEqual(expectedFromMethod.IsSpecial, requestResult.IsSpecial);
        }

        [TestMethod]
        public async Task GetByClientId_Should_Filter_By_ClientId_When_Api_Returns_Wrong_Results()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var expectedFromMethod = new DepartureToClient
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var returnedFromBase = new DepartureToClient
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23b"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var collectionName = "departureToClient";
            var args = new Dictionary<string, string>
            {
                { "clientId", clientId.ToString() }
            };
            var baseRepo = new Mock<IBaseApiRepository>();
            baseRepo
                .Setup(a => a.MakeRequest<DepartureToClient>(collectionName, args))
                .ReturnsAsync(returnedFromBase);

            var repo = new DepartureToClientRepository(baseRepo.Object);

            var requestResult = await repo.GetByClientId(clientId);

            Assert.AreEqual(null, requestResult);
        }
    }
}
