﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Api;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Unit.Repository.Api
{
    [TestClass]
    public class BaseApiRepositoryTests
    {
        [TestMethod]
        public async Task MakeRequest_DepartureToClient_Serialized_Properly()
        {
            var collectionName = "collection";
            var args = new Dictionary<string, string> { { "k1", "v1" } };


            var respFromWeb = @"{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : true
                        }";
            var expected = new DepartureToClient
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };

            var responseStream = StreamHelper.GenerateStreamFromString(respFromWeb.Replace('\'', '\"'));

            var response = new Mock<HttpWebResponse>();
            response
                .Setup(c => c.GetResponseStream())
                .Returns(responseStream);
            response
                .Setup(a => a.StatusCode)
                .Returns(HttpStatusCode.OK);

            var request = new Mock<HttpWebRequest>();
            request
                .Setup(c => c.GetResponseAsync())
                .ReturnsAsync(response.Object);

            var webRequest = new Mock<IWebRequest>();
            webRequest
                .Setup(a => a.Request(collectionName, args))
                .Returns(request.Object);


            var baseRepo = new BaseApiRepository(webRequest.Object);
            var requestResult = await baseRepo.MakeRequest<DepartureToClient>(collectionName, args);

            Assert.AreEqual(expected.IsSpecial, requestResult.IsSpecial);
            Assert.AreEqual(expected.LeaveFrom, requestResult.LeaveFrom);
            Assert.AreEqual(expected.LeaveTo, requestResult.LeaveTo);
            Assert.AreEqual(expected.ClientId, requestResult.ClientId);
        }

        [TestMethod]
        public async Task MakeRequest_DepartureToCompany_Serialized_Properly()
        {
            var collectionName = "collection";
            var args = new Dictionary<string, string> { { "k1", "v1" } };


            var respFromWeb = @"{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d23C',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : true
                        }";
            var expected = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23C"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };

            var responseStream = StreamHelper.GenerateStreamFromString(respFromWeb.Replace('\'', '\"'));

            var response = new Mock<HttpWebResponse>();
            response
                .Setup(c => c.GetResponseStream())
                .Returns(responseStream);
            response
                .Setup(a => a.StatusCode)
                .Returns(HttpStatusCode.OK);

            var request = new Mock<HttpWebRequest>();
            request
                .Setup(c => c.GetResponseAsync())
                .ReturnsAsync(response.Object);

            var webRequest = new Mock<IWebRequest>();
            webRequest
                .Setup(a => a.Request(collectionName, args))
                .Returns(request.Object);


            var baseRepo = new BaseApiRepository(webRequest.Object);
            var requestResult = await baseRepo.MakeRequest<DepartureToCompany>(collectionName, args);

            Assert.AreEqual(expected.IsSpecial, requestResult.IsSpecial);
            Assert.AreEqual(expected.LeaveFrom, requestResult.LeaveFrom);
            Assert.AreEqual(expected.LeaveTo, requestResult.LeaveTo);
            Assert.AreEqual(expected.ClientId, requestResult.ClientId);
        }

        [TestMethod] 
        public async Task MakeRequest_Response_Unserializable_Should_Return_null()
        {
            var collectionName = "collection";
            var args = new Dictionary<string, string> { { "k1", "v1" } };


            var respFromWeb = string.Empty;

            var responseStream = StreamHelper.GenerateStreamFromString(respFromWeb.Replace('\'', '\"'));

            var response = new Mock<HttpWebResponse>();
            response
                .Setup(c => c.GetResponseStream())
                .Returns(responseStream);
            response
                .Setup(a => a.StatusCode)
                .Returns(HttpStatusCode.OK);

            var request = new Mock<HttpWebRequest>();
            request
                .Setup(c => c.GetResponseAsync())
                .ReturnsAsync(response.Object);

            var webRequest = new Mock<IWebRequest>();
            webRequest
                .Setup(a => a.Request(collectionName, args))
                .Returns(request.Object);


            var baseRepo = new BaseApiRepository(webRequest.Object);
            var requestResult = await baseRepo.MakeRequest<DepartureToCompany>(collectionName, args);

            Assert.AreEqual(null, requestResult);
        }
        
        [TestMethod]
        public async Task MakeRequest_List_Of_DepartureToClient_Serialized_Properly()
        {
            var collectionName = "collection";
            var args = new Dictionary<string, string> { { "k1", "v1" } };


            var respFromWeb = @"[{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d231',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : true
                        },
                        {
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d232',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : true
                        },
                        {
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23c',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d233',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : true
                        }]";

            var expected = new List<DepartureToCompany>
            {
                new DepartureToCompany {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                }, 
                new DepartureToCompany {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d232"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },  
                new DepartureToCompany {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23c"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d233"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },    
            };

            var responseStream = StreamHelper.GenerateStreamFromString(respFromWeb.Replace('\'', '\"'));

            var response = new Mock<HttpWebResponse>();
            response
                .Setup(c => c.GetResponseStream())
                .Returns(responseStream);
            response
                .Setup(a => a.StatusCode)
                .Returns(HttpStatusCode.OK);

            var request = new Mock<HttpWebRequest>();
            request
                .Setup(c => c.GetResponseAsync())
                .ReturnsAsync(response.Object);

            var webRequest = new Mock<IWebRequest>();
            webRequest
                .Setup(a => a.Request(collectionName, args))
                .Returns(request.Object);


            var baseRepo = new BaseApiRepository(webRequest.Object);
            var requestResult = (await baseRepo.MakeRequest<IEnumerable<DepartureToCompany>>(collectionName, args)).ToList();

            Assert.AreEqual(requestResult.Count, expected.Count);

            for(var i=0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i].IsSpecial, requestResult[i].IsSpecial);
                Assert.AreEqual(expected[i].LeaveFrom, requestResult[i].LeaveFrom);
                Assert.AreEqual(expected[i].LeaveTo, requestResult[i].LeaveTo);
                Assert.AreEqual(expected[i].ClientId, requestResult[i].ClientId);
            }
        }
    }
}
