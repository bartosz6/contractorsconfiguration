﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Api;
using ContractorsConfig.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Unit.Repository.Api
{
    [TestClass]
    public class DepartureToCompanyRepositoryTests
    {
        [TestMethod]
        public async Task GetByClientIdAndCompanyId_Should_Filter_By_ClientId_And_CompanyId()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");
            var expectedFromMethod = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var returnedFromBase = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var collectionName = "departureToClient";
            var args = new Dictionary<string, string>
            {
                { "clientId", clientId.ToString() },
                { "companyId", companyId.ToString() }
            };
            var baseRepo = new Mock<IBaseApiRepository>();
            baseRepo
                .Setup(a => a.MakeRequest<DepartureToCompany>(collectionName, args))
                .ReturnsAsync(returnedFromBase);

            var repo = new DepartureToCompanyRepository(baseRepo.Object);

            var requestResult = await repo.GetByClientIdAndCompanyId(clientId, companyId);

            Assert.AreEqual(expectedFromMethod.ClientId, requestResult.ClientId);
            Assert.AreEqual(expectedFromMethod.CompanyId, requestResult.CompanyId);
            Assert.AreEqual(expectedFromMethod.LeaveFrom, requestResult.LeaveFrom);
            Assert.AreEqual(expectedFromMethod.LeaveTo, requestResult.LeaveTo);
            Assert.AreEqual(expectedFromMethod.IsSpecial, requestResult.IsSpecial);
        }

        [TestMethod]
        public async Task GetByClientIdAndCompanyId_Should_Filter_By_ClientId_And_CompanyId_When_Api_Returns_Wrong_Results()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");
            var expectedFromMethod = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var returnedFromBase = new DepartureToCompany
            {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23b"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d232"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
            };
            var collectionName = "departureToClient";
            var args = new Dictionary<string, string>
            {
                { "clientId", clientId.ToString() },
                { "companyId", companyId.ToString() }
            };
            var baseRepo = new Mock<IBaseApiRepository>();
            baseRepo
                .Setup(a => a.MakeRequest<DepartureToCompany>(collectionName, args))
                .ReturnsAsync(returnedFromBase);

            var repo = new DepartureToCompanyRepository(baseRepo.Object);

            var requestResult = await repo.GetByClientIdAndCompanyId(clientId, companyId);

            Assert.AreEqual(null, requestResult);
        }
    }
}
