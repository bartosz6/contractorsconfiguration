﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.File;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Unit.Repository.File
{
    [TestClass]
    public class BaseFileRepositoryTests
    {
        [TestMethod]
        public void BaseFileRepository_Fills_DataCollection_From_Configuration_Storage()
        {
            var configurationStorage = new Mock<IConfigurationStorage>();

            configurationStorage
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [
                     {
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23c',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : 'true'
                     }
                     ]");
            var expectedDataCollection = new List<DepartureToClient>
            {
                new DepartureToClient
                {
                    ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23c"),
                    LeaveFrom = TimeSpan.Parse("12:15"),
                    LeaveTo = TimeSpan.Parse("12:30"),
                    IsSpecial = true
                }
            };

            var baseRepo = new BaseFileRepository<DepartureToClient>(configurationStorage.Object);
            var dataCollection = baseRepo.DataCollecion.ToList();

            Assert.AreEqual(1, dataCollection.Count);
            Assert.AreEqual(expectedDataCollection[0].ClientId, dataCollection[0].ClientId);
            Assert.AreEqual(expectedDataCollection[0].LeaveFrom, dataCollection[0].LeaveFrom);
            Assert.AreEqual(expectedDataCollection[0].LeaveTo, dataCollection[0].LeaveTo);
            Assert.AreEqual(expectedDataCollection[0].IsSpecial, dataCollection[0].IsSpecial);
        }
    }
}
