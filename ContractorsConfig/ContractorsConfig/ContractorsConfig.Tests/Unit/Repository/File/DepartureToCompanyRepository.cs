﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Unit.Repository.File
{
    [TestClass]
    public class DepartureToCompanyRepository
    {
        [TestMethod]
        public async Task GetByClientIdAndCompanyId_Should_Filter_DataCollection()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23e");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");
            var dataCollection = new List<DepartureToCompany>
            {
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d230"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23e"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23c"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d232"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                }
            };
            var expected = dataCollection[1];
            var baseRepository = new Mock<IBaseFileRepository<DepartureToCompany>>();
            baseRepository
                .Setup(a => a.DataCollecion)
                .Returns(dataCollection);

            var repo = new ContractorsConfig.Repository.File.DepartureToCompanyRepository(baseRepository.Object);

            var result = await repo.GetByClientIdAndCompanyId(clientId, companyId);

            Assert.AreEqual(expected.IsSpecial, result.IsSpecial);
            Assert.AreEqual(expected.LeaveFrom, result.LeaveFrom);
            Assert.AreEqual(expected.LeaveTo, result.LeaveTo);
            Assert.AreEqual(expected.ClientId, result.ClientId);
        }
        
        [TestMethod]
        public async Task GetByClientIdAndCompanyId_Should_Filter_DataCollection_Asked_For_Non_existing_item_should_return_null()
        {
            var clientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23e");
            var companyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231");
            var dataCollection = new List<DepartureToCompany>
            {
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d230"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23b"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d231"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                },
                new DepartureToCompany 
                {
                ClientId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23c"),
                CompanyId = new Guid("9cf92466-2918-432b-8ce1-9e4997c3d232"),
                LeaveFrom = TimeSpan.Parse("12:15"),
                LeaveTo = TimeSpan.Parse("12:30"),
                IsSpecial = true
                }
            };

            var baseRepository = new Mock<IBaseFileRepository<DepartureToCompany>>();
            baseRepository
                .Setup(a => a.DataCollecion)
                .Returns(dataCollection);

            var repo = new ContractorsConfig.Repository.File.DepartureToCompanyRepository(baseRepository.Object);

            var result = await repo.GetByClientIdAndCompanyId(clientId, companyId);

            Assert.AreEqual(null, result);
        }
    }

}
