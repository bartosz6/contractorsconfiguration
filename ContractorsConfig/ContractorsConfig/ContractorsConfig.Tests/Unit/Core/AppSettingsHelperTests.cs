﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ContractorsConfig.Core;

namespace ContractorsConfig.Tests.Unit.Core
{
    [TestClass]
    public class AppSettingsHelperTests
    {
        [TestMethod]
        public void AppSettingsHelperTests_Create_Should_Fill_Key_Property()
        {
            var testKey = "testKey";

            var ash = AppSettingsHelper.Create(testKey);

            Assert.AreEqual(ash.Key, testKey);
        }
    }
}
