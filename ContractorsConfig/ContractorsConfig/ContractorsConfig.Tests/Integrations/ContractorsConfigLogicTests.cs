﻿using System;
using System.Threading.Tasks;
using ContractorsConfig.Core;
using ContractorsConfig.Model;
using ContractorsConfig.Repository.File;
using ContractorsConfig.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContractorsConfig.Tests.Integrations
{
    [TestClass]
    public class ContractorsConfigLogicTests
    {
        /// <summary>
        /// Theres no client in departureToClient with specified in departureToCompany clientId
        /// </summary>
        [TestMethod]
        public async Task INTEGR_ContractorsConfigLogicTests_Wrong_Data_In_Config__Should_Return_Null()
        {
            var configurationStorage1 = new Mock<IConfigurationStorage>();
            var configurationStorage2 = new Mock<IConfigurationStorage>();

            configurationStorage1
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23c',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : 'true'
                     }]");

            configurationStorage2
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d23C',
                     'leaveFrom' : 'PT15H15M',
                     'leaveTo' : 'PT16H30M',
                     'isSpecial' : 'true'
                     }]");

            var r = new ContractorsConfigLogic(
                new DepartureToClientRepository(new BaseFileRepository<DepartureToClient>(configurationStorage1.Object)),
                new DepartureToCompanyRepository(new BaseFileRepository<DepartureToCompany>(configurationStorage2.Object))
                );

            var res = await r.GetDepartureTimeByClientIdAndCompanyId(
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"),
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23C"));

            Assert.AreEqual(res, null);
        }

        /// <summary>
        /// All data correct, there is departure time for departureToClient, which should override departure time from departureToCompany
        /// </summary>
        [TestMethod]
        public async Task INTEGR_ContractorsConfigLogicTests_Times_For_Client_Departure_Present_In_Config__Should_Return_Departure_For_Client()
        {
            var configurationStorage1 = new Mock<IConfigurationStorage>();
            var configurationStorage2 = new Mock<IConfigurationStorage>();
                                 
            configurationStorage1
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : 'true'
                     },
                     {
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23c',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : 'true'
                     }]");

            configurationStorage2
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d23C',
                     'leaveFrom' : 'PT15H15M',
                     'leaveTo' : 'PT16H30M',
                     'isSpecial' : 'true'
                     }]");

            var r = new ContractorsConfigLogic(
               new DepartureToClientRepository(new BaseFileRepository<DepartureToClient>(configurationStorage1.Object)),
               new DepartureToCompanyRepository(new BaseFileRepository<DepartureToCompany>(configurationStorage2.Object))
               );          

            var res = await r.GetDepartureTimeByClientIdAndCompanyId(
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"), 
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23C"));

            Assert.AreEqual(res.LeaveFrom, TimeSpan.Parse("12:15"));
            Assert.AreEqual(res.LeaveTo, TimeSpan.Parse("12:30"));
        }
                   
        /// <summary>
        /// All data correct, there is no departure time for departureToClient, should return departure time from departureToCompany
        /// </summary>        
        [TestMethod]
        public async Task INTEGR_ContractorsConfigLogicTests_No_Times_For_Client_Departure__Should_Return_Departure_For_Company()
        {
            var configurationStorage1 = new Mock<IConfigurationStorage>();
            var configurationStorage2 = new Mock<IConfigurationStorage>();
                                 
            configurationStorage1
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'isSpecial' : 'true'
                     },
                     {
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23c',
                     'leaveFrom' : 'PT12H15M',
                     'leaveTo' : 'PT12H30M',
                     'isSpecial' : 'true'
                     }]");

            configurationStorage2
                .Setup(a => a.GetConfigString())
                .Returns(@"
                     [{
                     'clientId' : '9cf92466-2918-432b-8ce1-9e4997c3d23a',
                     'companyId' : '9cf92466-2918-432b-8ce1-9e4997c3d23C',
                     'leaveFrom' : 'PT15H15M',
                     'leaveTo' : 'PT16H30M',
                     'isSpecial' : 'true'
                     }]");

            var r = new ContractorsConfigLogic(
               new DepartureToClientRepository(
                   new BaseFileRepository<DepartureToClient>(configurationStorage1.Object)),
               new DepartureToCompanyRepository(
                   new BaseFileRepository<DepartureToCompany>(configurationStorage2.Object))
               );           

            var res = await r.GetDepartureTimeByClientIdAndCompanyId(
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23a"), 
                        new Guid("9cf92466-2918-432b-8ce1-9e4997c3d23C"));

            Assert.AreEqual(res.LeaveFrom, TimeSpan.Parse("15:15"));
            Assert.AreEqual(res.LeaveTo, TimeSpan.Parse("16:30"));
        }
    }
}
