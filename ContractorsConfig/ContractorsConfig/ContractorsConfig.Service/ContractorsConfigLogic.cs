﻿#region usings
using AutoMapper;
using ContractorsConfig.Repository.Interfaces;
using ContractorsConfig.Service.Infrastructure;
using ContractorsConfig.Service.Models;
using System;
using System.Threading.Tasks; 
#endregion

namespace ContractorsConfig.Service
{
    public class ContractorsConfigLogic
    {
        #region private fields
        private readonly IDepartureToClientRepository _departureToClientRepository;
        private readonly IDepartureToCompanyRepository _departureToCompanyRepository; 
        #endregion

        #region cstr
        public ContractorsConfigLogic(IDepartureToClientRepository departureToClientRepository, IDepartureToCompanyRepository departureToCompanyRepository)
        {
            _departureToClientRepository = departureToClientRepository;
            _departureToCompanyRepository = departureToCompanyRepository;

            AutoMapperConfiguration.Configure();
        } 
        #endregion

        #region public methods
        public async Task<DepartureTimeModel> GetDepartureTimeByClientIdAndCompanyId(Guid clientId, Guid companyId)
        {
            var departureToClient = await _departureToClientRepository.GetByClientId(clientId);
            var departureToCompany = await _departureToCompanyRepository.GetByClientIdAndCompanyId(clientId, companyId);

            if (departureToClient != null && departureToCompany != null)
            {
                if(departureToClient.LeaveFrom.HasValue && departureToClient.LeaveTo.HasValue)
                {
                    return Mapper.Map<DepartureTimeModel>(departureToClient);
                }

                if(departureToCompany.LeaveFrom.HasValue && departureToCompany.LeaveTo.HasValue)
                {
                    return Mapper.Map<DepartureTimeModel>(departureToCompany);
                }
            }

            return null;
        }

        public async Task<DepartureTimeModel> GetDepartureTimeByClientIdAndCompanyId(string clientId, string companyId)
        {
            return await GetDepartureTimeByClientIdAndCompanyId(new Guid(clientId), new Guid(companyId));
        } 
        #endregion
    }
}
