﻿using AutoMapper;
using ContractorsConfig.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractorsConfig.Service.Infrastructure
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<IDeparture, ContractorsConfig.Service.Models.DepartureTimeModel>();
        }
    }
}
