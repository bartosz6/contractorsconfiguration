﻿using System;

namespace ContractorsConfig.Service.Models
{
    public class DepartureTimeModel
    {
        #region props
        public TimeSpan? LeaveFrom { get; set; }
        public TimeSpan? LeaveTo { get; set; } 
        #endregion
    }
}
