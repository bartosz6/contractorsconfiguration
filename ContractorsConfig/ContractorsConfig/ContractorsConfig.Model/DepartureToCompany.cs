﻿#region usings
using ContractorsConfig.Model.Interfaces;
using System;
using System.Runtime.Serialization; 
#endregion

namespace ContractorsConfig.Model
{
    [DataContract(Name = "departureToCompany")]
    public class DepartureToCompany : ICompany, IDeparture
    {
        #region ICompany
        [DataMember(Name = "companyId")]
        public Guid CompanyId { get; set; }

        [DataMember(Name = "clientId")]
        public Guid ClientId { get; set; } 
        #endregion

        #region IDeparture
        [DataMember(Name = "leaveFrom")]
        public TimeSpan? LeaveFrom { get; set; }

        [DataMember(Name = "leaveTo")]
        public TimeSpan? LeaveTo { get; set; }

        [DataMember(Name = "isSpecial")]
        public bool IsSpecial { get; set; } 
        #endregion
    }
}
