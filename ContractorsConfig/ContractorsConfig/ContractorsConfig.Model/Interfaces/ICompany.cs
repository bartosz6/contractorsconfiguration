﻿using System;

namespace ContractorsConfig.Model.Interfaces
{
    public interface ICompany : IClient
    {
        Guid CompanyId { get; set; }
    }
}
