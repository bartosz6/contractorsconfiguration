﻿using System;

namespace ContractorsConfig.Model.Interfaces
{
    public interface IClient
    {
        Guid ClientId { get; set; }
    }
}
