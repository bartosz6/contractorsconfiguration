﻿using System;

namespace ContractorsConfig.Model.Interfaces
{
    public interface IDeparture
    {
        TimeSpan? LeaveFrom { get; set; }
        TimeSpan? LeaveTo { get; set; }
        bool IsSpecial { get; set; }
    }
}
