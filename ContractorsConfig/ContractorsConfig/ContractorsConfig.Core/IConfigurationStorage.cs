﻿
namespace ContractorsConfig.Core
{
    public interface IConfigurationStorage
    {
        string Key { get; }
        string GetConfigString();
    }
}
