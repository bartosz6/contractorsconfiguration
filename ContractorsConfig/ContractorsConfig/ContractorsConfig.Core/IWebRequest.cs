﻿
using System.Collections.Generic;
using System.Net;
namespace ContractorsConfig.Core
{
    public interface IWebRequest
    {
        HttpWebRequest Request(string collectionName, IDictionary<string, string> args);
    }
}
