﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ContractorsConfig.Core
{
    public class WebRequestHelper : IWebRequest
    {
        private readonly IConfigurationStorage _storage;

        public WebRequestHelper(IConfigurationStorage storage)
        {
            _storage = storage;
        }

        public HttpWebRequest Request(string collectionName, IDictionary<string, string> args)
        {
            var query = BuildQuery(collectionName, args);
            return WebRequest.Create(query) as HttpWebRequest;
        }

        public string ApiUrl
        {
            get { return _storage.GetConfigString(); }
            private set {}
        }

        private string BuildQuery(string collectionName, IDictionary<string, string> args)
        {
            return string.Format("{0}{1}?{2}", ApiUrl, collectionName, args.Aggregate(string.Empty, (a, b) =>
                a + string.Format("{0}={1}&", b.Key, b.Value)));
        } 
    }
}
