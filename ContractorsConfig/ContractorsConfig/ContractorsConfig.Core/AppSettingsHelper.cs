﻿#region usings
using System;
#endregion

namespace ContractorsConfig.Core
{
    public class AppSettingsHelper : IConfigurationStorage
    {
        #region cstr
        private AppSettingsHelper() { }
        #endregion

        public string GetConfigString()
        {
            if (!string.IsNullOrWhiteSpace(Key))
            {
                return System.Configuration.ConfigurationManager.AppSettings[Key];
            }

            throw new Exception("Use IConfigurationStorage.Create(key) to define key first.");
        }

        #region props
        public string Key { get; private set; } 
        #endregion

        #region static
        public static IConfigurationStorage Create(string key)
        {
            return new AppSettingsHelper
            {
                Key = key
            };
        } 
        #endregion
    }
}
